﻿CREATE TABLE [dbo].[W6MOBILE_REQUESTS] (
    [ClientRequestKey] BIGINT         NOT NULL,
    [CMUser]           NVARCHAR (100) CONSTRAINT [DF_W6MOBILE_REQUESTS_CMUser] DEFAULT (suser_sname()) NOT NULL,
    [Body]             NVARCHAR (MAX) NOT NULL,
    [ActivityName]     AS             (substring([Body],(2),(21))),
    [RequestDate]      DATETIME       CONSTRAINT [DF_W6MOBILE_REQUESTS_RequestDate] DEFAULT (getdate()) NULL,
    [MsgType]          INT            NULL,
    [MsgPriority]      INT            NULL,
    [MsgProcessed]     BIT            DEFAULT ((0)) NOT NULL,
    [SyncType]         INT            DEFAULT ((10)) NOT NULL,
    [ClientType]       INT            DEFAULT ((10)) NOT NULL,
    [RequestID]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Reserved1]        NVARCHAR (128) NULL,
    [Reserved2]        INT            NULL,
    [Reserved3]        BIT            NULL,
    [SectorID]         INT            NULL,
    CONSTRAINT [PK_W6MOBILE_REQUESTS_1] PRIMARY KEY NONCLUSTERED ([ClientRequestKey] ASC, [CMUser] ASC, [ClientType] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idxRequestDateCMUser]
    ON [dbo].[W6MOBILE_REQUESTS]([RequestDate] ASC, [CMUser] ASC);


GO
CREATE NONCLUSTERED INDEX [idxMsgProcessedCMUserRequestIDRequestDate]
    ON [dbo].[W6MOBILE_REQUESTS]([SectorID] ASC, [MsgProcessed] ASC, [CMUser] ASC, [RequestID] ASC, [RequestDate] ASC);


GO
CREATE NONCLUSTERED INDEX [idxCMUserMsgProcessedSyncType]
    ON [dbo].[W6MOBILE_REQUESTS]([CMUser] ASC, [ClientType] ASC, [MsgProcessed] ASC, [SyncType] ASC);

