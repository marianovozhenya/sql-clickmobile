﻿CREATE TABLE [dbo].[W6MOBILE_PUSH_REGISTERED_USERS] (
    [CMUser]             NVARCHAR (100) NOT NULL,
    [NotificationDate]   DATETIME       CONSTRAINT [DF_W6MOBILE_PUSH_REGISTERED_USERS_NotificationDate] DEFAULT (getdate()) NULL,
    [DeviceType]         INT            NOT NULL,
    [Token]              NVARCHAR (256) NULL,
    [DeviceId]           NVARCHAR (100) NOT NULL,
    [LogoutTime]         DATETIME       NULL,
    [LastConnectionTime] DATETIME       DEFAULT (getdate()) NULL,
    [Badge]              INT            DEFAULT ((0)) NULL,
    CONSTRAINT [PK_W6MOBILE_PUSH_REGISTERED_USERS_1] PRIMARY KEY NONCLUSTERED ([DeviceId] ASC)
);

