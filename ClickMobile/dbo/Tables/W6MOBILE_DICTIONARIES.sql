﻿CREATE TABLE [dbo].[W6MOBILE_DICTIONARIES] (
    [ItemKey]        BIGINT          NOT NULL,
    [CollectionId]   INT             NOT NULL,
    [Name]           NVARCHAR (100)  NOT NULL,
    [Body]           NVARCHAR (4000) NULL,
    [Condition]      NVARCHAR (50)   NULL,
    [GlobalRevision] BIGINT          NOT NULL,
    [Deleted]        BIT             CONSTRAINT [DF_W6MOBILE_DICTIONARIES_Deleted] DEFAULT ((0)) NOT NULL,
    [Reserved1]      NVARCHAR (64)   NULL,
    [Reserved2]      NVARCHAR (128)  NULL,
    [Reserved3]      NVARCHAR (256)  NULL,
    CONSTRAINT [PK_W6MOBILE_DICTIONARIES] PRIMARY KEY NONCLUSTERED ([ItemKey] ASC, [CollectionId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6MOBILE_DICTIONARIES_IDX1]
    ON [dbo].[W6MOBILE_DICTIONARIES]([CollectionId] ASC, [GlobalRevision] ASC, [Condition] ASC);

