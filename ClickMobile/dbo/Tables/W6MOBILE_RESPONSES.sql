﻿CREATE TABLE [dbo].[W6MOBILE_RESPONSES] (
    [W6Key]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [ClientRequestKey] BIGINT         NOT NULL,
    [CMUser]           NVARCHAR (100) NOT NULL,
    [Body]             NVARCHAR (MAX) NOT NULL,
    [ActivityName]     NVARCHAR (50)  NULL,
    [RequestDate]      DATETIME       CONSTRAINT [DF_W6MOBILE_RESPONSES_RequestDate] DEFAULT (getdate()) NULL,
    [ResponseDate]     DATETIME       CONSTRAINT [DF_W6MOBILE_RESPONSES_ResponseDate] DEFAULT (getdate()) NOT NULL,
    [MsgType]          INT            NULL,
    [MsgPriority]      INT            NULL,
    [ClientType]       INT            DEFAULT ((10)) NOT NULL,
    [Reserved1]        NVARCHAR (128) NULL,
    [Reserved2]        INT            NULL,
    [Reserved3]        BIT            NULL,
    [PushText]         NVARCHAR (256) NULL,
    [PushFlag]         BIT            DEFAULT ((0)) NULL,
    CONSTRAINT [PK_W6MOBILE_RESPONSES] PRIMARY KEY CLUSTERED ([W6Key] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_W6MOBILERESPONSES_ClientType_MsgType_CMUser]
    ON [dbo].[W6MOBILE_RESPONSES]([ClientType] ASC, [MsgType] ASC)
    INCLUDE([CMUser]);


GO
CREATE NONCLUSTERED INDEX [idxCMUserClientTypeReserved2]
    ON [dbo].[W6MOBILE_RESPONSES]([CMUser] ASC, [ClientType] ASC, [Reserved2] ASC);


GO
CREATE NONCLUSTERED INDEX [idxCMUserW6Key]
    ON [dbo].[W6MOBILE_RESPONSES]([CMUser] ASC, [ClientType] ASC, [W6Key] ASC);


GO
CREATE NONCLUSTERED INDEX [idxMsgTypeUserMsgPriority]
    ON [dbo].[W6MOBILE_RESPONSES]([MsgType] ASC, [CMUser] ASC, [MsgPriority] DESC)
    INCLUDE([ClientType]);


GO
CREATE NONCLUSTERED INDEX [W6MOBILE_RESPONSES_IDX2]
    ON [dbo].[W6MOBILE_RESPONSES]([ResponseDate] ASC);


GO
CREATE NONCLUSTERED INDEX [W6MOBILE_RESPONSES_IDX1]
    ON [dbo].[W6MOBILE_RESPONSES]([CMUser] ASC);

