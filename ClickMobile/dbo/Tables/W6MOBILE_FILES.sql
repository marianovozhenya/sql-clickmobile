﻿CREATE TABLE [dbo].[W6MOBILE_FILES] (
    [FileKey]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [FileName]         NVARCHAR (50)  NOT NULL,
    [FileBinary]       IMAGE          NOT NULL,
    [FileSize]         BIGINT         NOT NULL,
    [DateStamp]        DATETIME       CONSTRAINT [DF_W6MOBILE_FILES_DateStamp] DEFAULT (getdate()) NOT NULL,
    [FileVersion]      INT            NULL,
    [ClientType]       INT            DEFAULT ((10)) NOT NULL,
    [Reserved1]        NVARCHAR (128) NULL,
    [Reserved2]        INT            NULL,
    [Reserved3]        BIT            NULL,
    [ObjectType]       INT            NULL,
    [ObjectKey]        INT            NULL,
    [ObjectPropertyId] INT            NULL,
    [MVIndex]          INT            NULL,
    CONSTRAINT [PK_W6MOBILE_FILES_1] PRIMARY KEY CLUSTERED ([FileKey] ASC)
);


GO
CREATE NONCLUSTERED INDEX [W6MOBILE_FILES_IDX1]
    ON [dbo].[W6MOBILE_FILES]([FileName] ASC, [FileSize] ASC);

